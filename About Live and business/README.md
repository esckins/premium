First of all let's start with a great sentence.

**What You Got here, Won’t Be Get There**

Yes, as written.. If you want to continue cycling, you mustn't stop pedaling.

> This article will not have an introduction, progress and result.

`People need two things to work with you:`

1. To trust you, that is, that you will not stab them in the back.
2. That you can do your job, that is, that you are an expert.


`If you have these two things, don't expect people to mystically understand it. Show them.`

Don't run away from conflict, don't be afraid to lose. Some fights must break out, some hearts must be broken, and some people must be lost. It's life's way of renewing itself.

OK ..
.. alright; What is required for effective communication?

`Basically, effective communication has 3 steps.`

1. What? What is the topic?
2. So What? Why is it important? Why should it concern me or us?
3. What Now? If so, what should I (should) do?

you can measure your communication level in meeting.

**Meetings**; It consists of introduction, progress and result phases. Every part is equally important.

`Entry`

If there are other problems in the minds of the participants at the beginning of the meeting, they can be talked for a few minutes to solve them or hello can be said to relax the atmosphere and attract attention.

if you are a foreigner in the environment; in the beginning; try to get people to talk more than yourself.
How is the weather there today? I think there is a good question firstly.


`Progress`

It is the section to discuss the issue in the progress phase of the meeting process.
In this chapter; Irrelevant topics should not be included. If the subject is skipped, the purpose is reminded, the subject is returned and the stages are passed.

Repetitions are not allowed for agreed cases. It is useless for 3 people to say the same thing or for one person to say the same thing 3 times.
At the end of the day, actions must be taken. There should be a responsible, due date and follow-up for actions.

`Result`

I think meetings should last 45 minutes max. Everyone should understand the actions. Think of your team as a chain. Almost, you have common problems and you should make an effort to solve them. The musicians died when the Titanic sank.

```
Microsoft docs, azure devops, jira, google chat, excel.. can be used to note actions and for meeting process
In some companies, the cost of the meeting is also shared so that its value can be understood and we can appreciate the time.
```

If you're usually on the audience's side at meetings, there's a problem. Of course, luck will come to you many times over. Do you want to score or return to the goalkeeper?

Of course, this will be different for everyone you know. Think about it. If there is nothing at the table that improves you, you are at the wrong table.
You must know how to get up.. if you're improving, why not use it.

> In making money, it is the effect that determines the result not the effort. Working hard doesn't determine how much you earn; The size of the work you do determines the return to the company, that is, the impact. The more impact and risk, the more profit. Of course, it takes a lot of work beforehand to have the capacity to have this effect.


https://eksisozluk.com/kaan-aslan--264904?p=2


https://www.numbeo.com/cost-of-living/in/Sofia
