## Kaynaklar genelde rol modellerdir. Bknz. Tarik Güney

[rc](https://twitter.com/atarikguney)

### Stres hkk.
```
Stres çoğumuzun problemi. Nedenleri bilindiğinde çözüm de ona göre olur:

1. Çok fazla çalışmak
2. Uykusuzluk
3. Kötü beslenme
4. Yöntemsiz bir takımda çalışmakla devamlı hata düzeltmek zorunda kalmak
5. İstenilenlerin açık olmaması
6. Panik modunda idareciler
7. İşinizi bilmemek
16. Kontrolsüz öğrenme
17. Sadece yazılım düşünüp konuşmakla meslektem bunalıp sıkılma
18. Arkadaşsızlık, yalnızlık
19. Bilmediğimiz bir ortamda olmak

Bunlar ve daha fazlası stres nedeni olabilir. 
Kaynağı bulmadan su çıkarılmaz ki maalesef.
```

---

```
Sorumluluklarım arttıkça, ve kendime koyduğum bazen mecburi bazen de keyfi hedeflerim zorlaştıkça, dramaya karşı sabrım da o denli düşmeye başlıyor.

Zamanla dramadan kaçmak, bir fantezi değil, ciddi bir ihtiyaç halini alıyor. 

Keyfi bile olsa, ona harcayacak enerjiniz kalmıyor.

Dramadan kastım: 

Devamlı olumsuz düşünen, 
dert üretip duran, 
her sorunu olduğundan büyüten, 
hayatın şartlarını anlamamış sonrasında her şeye kızıp küsen, 
ve etrafındaki insanlara da bunu yaşatanların sergiledikleri tavırlar.

Artık güzel görmeyi öğreten insanları mı okursunuz, manastıra ya da inzivaya çekilip hayatı mı sorgularsınız, artık ne işe yarıyorsa, onu yapıp, her derde aşkla yapışıp, etraftaki herkesi de bununla muzdarip etmeyi bırakmak lazım.

Bazı dertler zor ama her dert zor değil.
```

### Dinlemek hkk.

```
Bolca dinlemek, sorunların çözmede şöyle yardımcı olur:

1. Karşıdaki, konuştukça sorunların basitliğini farkedebilir.
2. Karşıdaki, sorunlarından bahsederken çözümü de bulabilir.
3. Ya da siz, dinlemek ile sorunun tam olarak nerede olduğunu görür, kaynağı düzeltirsiniz.

Bazen de siz cevabı bilmiyor olabilirsiniz ama karşıdaki sizden cevap almaya gelmiş olabilir. 

Bu tür durumlarda, hemen bilmiyorum demek yerine, bırakın karşıdaki anlatsın. Eğer yukarıdaki ilk iki şık gerçekleşirse, zaten bilmiyorum demenize gerek kalmaz.

Ya da gerçek sorun, zannettiğiniz yerde değil de, belki de aslında başka bir yerdedir ve orası sizin bilgi alanınıza giriyor olabilir.

Onun için hemen bilmiyorum diye kestirip atmak faydasız olur.

Hala bilmiyorsanız, en azından dinlemiş olursunuz, bu da karşıdakine saygıdır.
```


---

```
İş yerinde herkesle arkadaş olmaya çalışmak stresi yıpratıcı olabilir. 

Profesyonel ve kibar davranmak ile arkadaş olmak aynı şeyler olarak görülmemeli.

Bu ikiyüzlü olmak değil. İnsanlar ile ortak bir sorun üzerine çalışmak ile ortak duyguları paylaşmak aynı değil.

Hatta, dürüst olmak adına kaba ya da somurtkan durmakdan ve insanları germekten daha güzel bir yaklaşım var:

İlla da insanlara gülümsemek, onlara yardım etmek için onlar ile özelinizi paylaştığınız arkadaşlık seviyesine çıkmaya gerek yok.

```


### Tartışma Kültürü hkk.

Devamlı hatalar ile uğraşmak istemiyorsanız, takımlarınızda sağlıklı bir tartışma kültürünü oluşturmanız gerekir.

İşlerin biraz yavaşlaması ile devamlı fazlaca hata çıkması yönetimde aynı etkiye sahip değildir. 

Takımın birbirlerine sordukları güzel soruları övebilirsiniz.

### Kariyer hkk.

```
Kariyerinizde sırayla şu üç aşamadan geçeceksiniz:

1. Ne yapılması gerektiğini bildiğiniz aşama.
2. Çok farklı şey yapmaktan neyi başardığınızı bilmediğiniz aşama.
3. Önceliklendirmeyi öğrendiğinizden, neleri başardığınızı yeniden bilmeye başladığınız aşama.

Çoğumuz ikinci aşamada korkmaya ve stress yaşamaya başlarız. 

Aslında o, doğru yolda olduğumuzun göstergesidir.

Hiç kimse, her şeyi süper bilip ondan sonra yapmaz. 

Kafanız karışıyor, sınırları göremiyorsanız, öğreniyorsunuz demektir.

Yakında, o sınırlar yeniden görülür olur.
```


### Daha iyisi hkk.

```
Daha iyisi için neden gerekir:

1. Kullanıcılar artıyordur ve daha iyisi yazılmadıkça hem kod hem de hız ölçeklenemiyordur.
2. Projede birden fazla mühendis çalışıyordur.
3. Kod çok hatalıdır.

Bunlardan hiç birisi yoksa, refactor gereksiz olabilir. Asıl olan üründür, kod değil.

Deliver edilmeyen her proje değişen market karşısında zamanla erir ve değersizleşir.

```


---

```
Bir ülke, şirket, takım nasıl batar?

Yöneticilerin etrafında korkudan ve/veya menfaatten dolayı doğruları söyleyenler kalmaz. 

Neden kalmaz?

Çünkü, yöneticiler kibirli, cahil ve zorba kişilerdir. 

Cehaletleri, gücü devamlı daha fazla mal toplamak zannetmektir.

Devamlı istediklerini duydukları için, kendilerini aslında olduklarından daha güçlü zannetmeleriyle sonuçlanır.

Zamanla boylarından büyük işlere kalkışırlar.

Sonra boylarının ölçüsü, hayal bile edemedikleri kayıplar ile alınır.

Zaman alır sadece cezalarını çekmeleri.

Bu yöneticiler, kendilerine söyledikleri yalanlara o kadar çok inanmışlardır ki, bu özgüvenleri, kendilerini dinleyen halkı veya çalışanları bile kandırabilir.

Ortam, bu şartlarda zamanla tımarhaneye dönüşür. 

Herkes yalanların inanılmazlığı ölçüsünde onlara inanmaya başlar
```
---

`İnsanların güvenlerini, insana saygılarını, huzur isteklerini, duygularını, hayallerini sömürmenin zeka ile açıklanabilir bir tarafı yok. Herkes isterse birilerini sömürebilir, kandırabilir. Zeka, hileye başvurmadan başarabilmek belki de. Ama her ne ise, bu zekilik değil.`

---

```
İleride mülakatlarda kullanmak için yaşadığınız zorlukları not alın ve ufak hikayeler haline dönüştürün. Faydaları:
1. Unutmanın önüne geçersiniz.
2. Dersler çıkarmanıza yardımcı olur.
3. Zorluklar ve can sıkıntıları, isimleri konulduğu zaman hafifler.

Mülakatlarda, bildiğiniz kadar değil, hazırlandığınız kadar başarılı olursunuz.

Farkında olmadan çoğumuz geçmişimizde, özellikle de mülakatlarda davranışsal sorulara cevap olarak kullanabileceğimiz, tecrübeler yaşamız oluyoruz. 

Ama o kadar emek ile kazandığımız o anları unutup gidiyor, onlardan ders çıkarmıyoruz.

Mesela, delegation gibi teorik bilginin sorunu, geçmişten bir örnek anlat denildiğinde hemen aklımıza bir cevap getirmiyor olması:

Onun için, delegation(yetkilendirme)'ı istenilen şekilde yapmış gerçek bir örnek duymak:

1. Ya geçmişimizde yaptığımız bir örneği aklımıza getirebilir.
2. Ya da yaptığımızı daha iyi çerçeveletebilir.

Örnekler üzerinde oturup düşünmeli ve notlar alınmalı. 

Olayı teorik kısımda bırakmayın. Delegation nedirin cevabı çok da önemli değil, önemli olan yapmış olmak.

```

---

```
Bazen insanlar, ödedikleri miktar değil, karşıdakinin kazandığı miktar açısından fiyatı belirlemek istiyor. 

Mesela mantıkları: Bu ürün 2 dolar ve bunu ödeyebilirim. Ama bunu satan, bundan 100 bin satıyorsa, 200 bin kazanıyor. Çok kazanıyor. O zaman 2 dolar fazla. Bedava olsun.

Karşıdakinin ne kazandığını düşünüp moral bozmaktan, kendisine sunulan ucuz imkanı değerlendirmeyi akıl edemiyor, fırsatı kaçırıyor.

Bilmiyor ki, bedava olan teşvik etmez, kalite bozulur. 

Bilmiyor ki, para vermeden para kazanılmaz. 

Üniversite için para veriyor, işe gitmek için para veriyor, kitap okumak için para veriyor.

Bazen pahalı olan satın alınan değil, alışkanlıklarımızdır.

Bazılarının kaybetmesi, bilgiye bakkaldan aldıkları bir çikolata kadar değer vermemelerinden.

Bilen insan, bilginin değerini anlayandır. 

Yüzlerce kaçak kitaplar indirir, hiç birini okumaz. 

Bedava değersizleştirir. Onun yazılması için harcanan zamanı ve emeği önemsizleştirir.
```


---

```
Başarılı takım ve şirketlerde gördüğüm benzerlikler:

1. Üretmeden eleştirenler ile çalışmamak.
2. Pozitif kalmak ve değişimden korkmamak.
3. Anlaşılır olmak ve anlatılanı da anlamak.
4. Aksiyonu, çok konuşmaya tercih etmek.

Bunlar yoksa teknik bilgi işe yaramıyor.

Birinci tipler, sadece gürültü yaparlar. Takımların odağını bozarlar.

İkinciler, başarının değişimle geldiğini bilirler ve bunu merakla karşılarlar.

Üçüncüler, işin kolay ve sorunsuz olmasını sağlarlar.

Dördüncüler, bazı şeylerin ancak yapılarak anlaşılabileceğini bilirler.
```

```
Neden üretmeden eleştirmek eksiktir, neden daha iyi bilmek için üretmek gerekir?

En temelde, sizi hayal dünyasından gerçek dünyaya getirir.

Bir işin zor olmasını bilmek ile zorluğun şiddetini yaşayarak anlamak ve sevdiğiniz her şeyden fedakarlık yapmak aynı şeyler değildir.


Üretenler, yolun çilesini bilirler. Sadece çözümü değil, çözüme giden yolu yani başarılması gereken her adımı da verimli hale getirmek için uğraşırlar.

Verimli hale gelmeyen adımlar, zamanınızdan ve paranızdan, değer verdiklerinize ve kendinize ayırmak istediklerinizden ödenir.

Bu bedeli ödemeyenler için sürecin zorluğu fikirdir. 

Tok acın halinden anlamadığı gibi, üretmek sürecinin yorgunluğunu bilmeyenler, bu süreçte alınan kararların nedenlerini de tam anlamazlar.

Onun için, akıl herkesten alınmaz. Akıl alacaksınız, o yolu geçmiş insanlara danışın.

Üretmeden devamlı eleştirenler, bozuk saat gibidirler. 

Günde en az bir kez doğruyu söylerler. 

Ama 23+ saat boyunca boş laflar dinlemek zorunda kalırsınız. 

Vaktim yok diyen, aksiyon alanlara baksın.

Üretenlerin hataları bile diğerlerinin başarılarından üstündür.



```


```
Ownership, yani yaptığınız işi sahiplenme, değer üretene önem veren şirketlerde kariyeriniz üzerine çok etkili bir anlayış olacak.

Arzuladığınız maaşları alabilmek için ise, çalışanlarının ürettikleri değere önem veren şirketler ile çalışmanız gerekir.

En basitinden, sahiplenme, uzun vadeli düşünmek ve uzun vadeli kazanımları kısa vadeli hedeflere kurban etmemektir.

Yaptıklarını sahiplenenler takımlarının öteside, şirketleri adına davranırlar.

Benim işim değil bu demezler.
```



```
Bir konuyu daha iyi öğrenmek için zihnin bir süre o konu hakkında aktif tutulması lazım. 

Zihnin arka planda bu konuyu düşünmesi demek bu.

Bunu ben şöyle sağlıyorum:


Daha iyi anlamak istediğim konu hakkında düşünmek istediğimde, onunla alakalı bir kaç not alırım.

Sonrasında, bu her gün bir ya da iki defa yazdıklarıma bakar, sonrasında bu notlara yeni şeyler eklerim.

Bu sürede aklıma ilginç düşünceler gelir. Onları da yazarım.


Ama bu yanınızda not defteri taşımanız demek.

Yazmaya üşeniyorsanız, bu iş olmaz.

Not defteri taşımaya üşeniyorsanız, bu iş olmaz.

Her yerde ve her anda yazdırabilecek bir not defteri olsun ve kompleks notları hızlıca aldırsın. 

Telefon ve iPad bu ikisini de karşılamıyor.


Klavye ile daha hızlı yazmanıza yardımcı olacak ufak bir yöntem:

Yazdıklarınızı backspace ile harf harf silmek yerine, kelime kelime silmek için:

1. CTRL + Backspace --> Windows
2. Alt + Backspace --> MacOS

Satırı silmek için: 
1. Command + Backspace -- MacOS
```


```
Burnout, yani tükenmişlik yaşadığınızın potansiyel 7 göstergesi:

1. İşleri erteleyip durmak
2. İnsanlardan kaçmak
3. Sabırsızlaşmak
4. Uyuyamamak
5. Eski hobilerinizin zevk vermemesi
6. İş performansının düşmesi
7. Kafa dağıtmak için bağımlılık yapan alışkanlıklara yönelmek


5. ve 6. maddeler aynı anda kendilerini şöyle gösterebiliyor:

Film ya da dizi izlerken, devamlı telefona bakmak.
Sosyal medyada fazla zaman geçirmek.

Çalışma zamanınızın %10’nunu daha doğru ve verimli çalışma yöntemlerine ayırın. 

Çok çalışmak, çekici olanın her şeyi çivi zannetmesine benzer.

Neden? 

Zamanla daha az çalışmakla daha çok üretmek için. 

1. Mesleki kitaplar
2. Automation
3. Arada bir en başa dönüp, sorgulamak.

Burnout yaşamamızın en büyük nedenlerinden birisi çok çalışmak.

Beşinci viteste bayır çıkmaya çalışmak gibi.

Bunu kimse sizin için düşünmeyecek. Vitesi sizin değiştirmeniz lazım.
```


```
Burnout'dan çıkmanın genel bir yöntemleri var. Ben kendi yaptıklarımdan bahsedeyim. Belki size de yardımcı olur:

1. Şirket istediği için mi, şahsi sorumluluklarımdan kaçmak için mi çok çalıştığımı sorguladım ve ikincisinin daha fazla olduğunu gördüm. İşi işte bıraktım. İş bitmez

2. Sevdiğim kitaplar ile daha fazla zaman geçirmeye başladım. Kitaplar sadece yeni şeyler öğrenmek için değil, aynı zamanda bildiğiniz şeyleri size birilerinin devamlı hatırlatmasıdır.

3. Sevdiklerim ile daha çok zaman geçirmeye, onları kendimi zorlayarak da olsa daha aktif dinlemeye başladım. 

4. Yaptığım işleri bitiremediğimde, kafamdan çıkaramadığımı gördüm ki bu herkeste olan bir sorun. Kafamı boşaltmak için yarım kalan işlerimi kısaca yazmaya başladım.

5. Tasarruflu olacağım derken, hayatın temel zevklerinden kaçmamaya başladım. Güzel bir yemeğe, güzel bir kitaba, ya da kullandığınız araçların daha kaliteli olanlarına karşı biraz daha fazla harcamaktan çekinmedim. Kaliteli şeyler kullanmanın tecrübesi ayrı bir zevkli.

6. Decision Fatigue, yani gün içinde devamlı karar almaktan dolayı, yani karar alamayacak kadar yorgunluk yaşamak bu zamanda çok kolay. 

Bu yorgunluğu yaşadığınızda, eşinizin ve çocuklarınızın, kısacası herkesin sorduğu soruları cevaplamak dünyanın en zor şeyi olmaya başlıyor.

6.1. Yapacağınız işleri dikkatli seçerek, iş yaparken 10 farklı şeye odaklanmayarak bunu azaltabilirsiniz. 

Birden fazla işe odaklanmak insanı çok hızlı yorar. Bir işi bitirin, sonra diğerine geçin.

7. Dediklerim bir konuda yardımcı olmayacak. O da, içinde yaşadığınız ülkenin ekonomik ve siyasi iklimi. 

8. Twitter'da 6 kişiyi takip etmemin sebebi, farkında olmadan bizleri delicesine yoran bilgi akışını, özellikle de kanıtsız bilgi alkışını engellemek.

9. Canım istemediğinde bile ailemle dışarıya daha fazla çıkmaya çalışmak. Çıktıktan sonra kendimi %100 daha iyi hissedeceğimi biliyorum. Bunu kendime hatırlatınca çıkmak istememek daha kolay kontrol edilebiliyor.

10. Planlar ile olacaklar farklı olabilir. Bunu baştan kabullenmek

11. Kendinize zorla da olsa arkadaşlar bulmak. Hiç tanımadığım insanlar ile tanışmaya başladım. 

Herkesin, kendim gibi kapandığı bir zamanda, insanları buluşturma yöntemleri buldum. 

Mesela, cuma günleri diğer babalar ile çocukları alıp düzenli buluşmaları ayarladım.

12. Beden işçisi insanların çalışma ortamlarında, şuraya yaklaşmayın, buna eldivensiz dokunmayın, vs. gibi onlarca uyarı olur. 

Bunlar beyin işçisi olan bizlere de olması lazım:
1. Düşmemek için birden fazla işe odaklanmayın.
2. Not almadığınızda elektrik çarpabilir.
3. vs.

13. Yaptığım işi daha iyi öğrenmeye başladım. Mesleği devamlı en kritik anlarda öğreniyorsanız, bu sizi çok yorar. 

Başkalarının tecrübelerini günde bir kaç sayfa okumak ile bile öğrenebilir ve kritik zamanları çok daha rahat yönetebilirsiniz.
```


---

```
Neden aktif olarak öğrenmek önemli?

Kendi notlarımdan küçük bir parça:

1. Aklın meşgul tutulması ve konu hakkında çıkarımlar yapması için.

2. Size devamlı doğruları hatırlatmakla onları içselleştirmenizi sağlayan bir rehber olması.


```


```
Sistem dizayn mülakatına girecek olan insanlara bu videoyu kesinlikle izlemelerini tavsiye ederim.

1. Nasıl sorular sorulmalı
2. Kapasite planlaması
3. Varsayımların yazılması
4. Ne tür veritabanları kullanılabilir
5. Sistem nasıl ölçeklenir

tiktok system dizayn

https://www.youtube.com/watch?v=07BVxmVFDGY&ab_channel=GauravSen


```

```
Sistem tasarımlarında sıkça kullanılan Fault Tolerant ve High Availability aslında farklı şeyler.

HA %99 - %99.99x arasında gidip gelirken, Fault Tolerance 100% uptime\availability sözü verir.

HA tasarlamak maliyet açısından daha ucuza gelebilir.
```


```
Piyasada latency genelde response time yerine kullanılıyor. 

Aynı şey değiller.

Latency, yani latent yani dormant, bir mesajın işleme alınana kadar geçen süre. Mesela, kablolar üzerinde seyahat, disk arası gidip gelmeler…

Response Time = Latency + Message Processing Time

Response Time, bir isteğin kullanıcıya cevap olarak dönmesine kadar geçen süre.

Ama latency’i düşürdüğünüz de Response Time’da büyük ölçüde düşürülebilir.
Çok duyarsınız:

High Throughput
Low Latency Read and Writes


```


```
Ufak şeylerden zevk alabilmek bile insanın hayatını adamasını gerektiriyor.

Ya çok okumakla kainatın derinliklerini anlamayı,

Ya ruhen devamlı kendini derinleştirmeyi,

Ya da arzuladığın şeylerin peşinden koşmakla hayal kırıklıkları ve yorgunluk yaşamayı gerektiriyor...

Ufak şeyler, kalbinizdeki yaralara merhem  oldukça zevk veriyor.

Bunun için ya yaralarınızın farkına varacak ve acizliğinizi göreceksiniz,

Ya da bunları sizlere gösterecek serüvenlere atılacaksınız...


```

```
Bir kaç kişinin hakkınızda yaptıkları ölçüsüz eleştirilerden daha kompleks ve bütüncül varlıklar olduğunuzun ve hemen yıkılmamanın farkında olmak lazım. 

Bazen kötü eleştirilerin bizlerde yankı bulmasının sebebi, kendimiz hakkındaki olumsuz düşüncelerimizi seslendirmelerinden.


```

### Yönetici olm. isteyenlere

```
Yönetici olmak isteyenlere tavsiye:

Robert California’nın özgüveninin yarısına bile sahip olsanız, bu meslekte bayağı başarılı olabilirsiniz.

Takım yönetiminde, 

Sizin emin olmadığınız kararlardan takım da emin olmaz. 

Sizin heyecan duymadığınız kararlara, takım da heyecan duymaz.

Sizin ciddiyet göstermediklerinize, takım da ciddiyet göstermez.

Yarım ağızla verdiğiniz kararlar ve tavsiyeler, bir kulakla dinlenir.


Açıklık sağlamak, sadece konuşmaları sadeleştirmek ile olmayabilir. Tavrınızın da net ve açıklık getirecek halde olması lazım.

Eğer motivasyon eksikliği yaşayan bir takımınız varsa, önce kendinize bakın.

Anlattıklarınıza siz inanıyor musunuz? İnanmışlığın netliği var mı?
```



```
Kariyerimin bu noktasında önceden tanımlı soruların sırayla sorulduğu mülakatları değil, iki kişinin muhabbet ettiği ve yaşanılan sorunları çözüp kendi geçmişlerinden örnekler verdiği mülakatları daha çok seviyorum.



```

```
Mülakatlarda sormasalar bile cevaplamanız gereken asıl soru: Şirkete ne katacaksın?

Bunu cevabı kalitenizi, üzerine bina ettiğiniz özellikleriniz? Yani Core Strenghts. 

Peki bunların ne olduğunu nasıl bulursunuz?

Şunları sorun:

1. Neyi yapmayı seviyorum?
2. İnsanlar beni hangi özelliklerimden dolayı övüyorlar?
3. En mutlu olduğum anlar neler?
4. Beni başkalarından farklı kılan nedir?

Örnek olması için, ben:

1. Organize bir insanım.

2. Enerjili ortamları severim. (Herkesin terlediği ortamlardan bahsetmiyorum; fikirsel.)

3. Anlaşılır ortamları severim ve bu ortamları oluştururum.

4. Sonuç üretmekten zevk alırım.

5. Sormaktan ve öğrenmekten çekinmem.


---
Şirkete sorular

1. Günlük production incident sayınız nedir?
2. Şimdiye kadar beni tanıdığınız kadarı ile, şirket kültürüne ters düşen bir yaklaşımım var mı?
3. Hata sonrası gösterilen tepkiler nasıl oluyor?
4. Takımlarda motivasyon nasıl sağlanıyor?

---

Seni başkalarından ayıran özellikler nedir sorusundaki başkalarını, dünyaya kainatın yaratılışından beri gelmiş geçmiş insanlar olarak anlamayın.

Bazı örnekler:

1. Çalıştığınız ortamlarda çoğu insan, bu benim işim değil derken, siz tersini düşünüyor olabilirsiniz.
2. Ya da herkesin konuşmaya çekindiği ortamlarda konuşabilme cesaretini gösterebiliyorsunuzdur.
3. Kötü eleştirilere takılıp kalmak yerine, doğru bildiğiniz yolda yürüme cesareti gösterebiliyorsunuzdur.



```


```
Artık SQL, NoSQL derken, yeni veri tabanı NewSQL son zamanlar karşımızda.

NewSQL, dağıtık ACID garantileri sunuyor. 

Örnekler:
1. Google Spanner
2. YugabyteDB
```


```
Size ufak bir tavsiye vereyim:

Kendinizi asla, biliyorum ama anlatamıyorum bahanesi arkasına saklamayın.

Eksikler, ortası boş bir duvar gibidir. Arkalarına saklanmakla görünmez olmazsınız.


```

```
Uzmanlıkla gelen en büyük sorunların başında alternatif fikirlere karşı başlayan körleşme oluyor. 

Onun için, hala ilk günün acemiliği hissetmek, farklı fikirlere karşı açık olmak ve başkalarının akıllarını kullanmak gerekiyor.
 
Tabi burada başkaları, ehil ve kaliteli insanlar.
```


```
Sevmedikleri insanları bitirmek için bazılarının yapmadıkları şey kalmıyor.

Halbuki tek yapmaları gereken, o insanların yanlışlarını övmek ve hatalarına devamlı bir kılıf bulmak.

Eğer birisi size bunu yapıyorsa, sonunuz pek hayırlı olmayacak demektir.


```



### Başarılı insanlar hkk.

```
Başarılı ve daha başarılı olma potansiyeline sahip kime baktıysam, gördüğüm en belirgin özelliklerin başında, düşüncelerinde ve inandıklarında kristal bir belirginliğin olması.

Çünkü:

1. Neyi neden yaptıklarını iyi biliyor ve bunu kolayca ifade edebiliyorlar.
2. Yaptıklarına açıklık getiriyorlar. Karmaşıklığı, “neden” soruları ile azaltıyorlar.

3. Her şeyi olduğu gibi kabul etmiyorlar. Daha anlaşılır olan alternatiflerden korkmuyorlar.

4. Bolca not alıyorlar. Notlar ile akıllarındaki karmaşıklığı gideriyorlar.

5. Basit planlar yapıyorlar ve bunları kesinlikle aksiyona dönüştürüyorlar.

6. Önceki planlarının ve düşüncelerinin üzerinden tekrar tekrar geçip basitleştirmeye devam ediyorlar.

7. Basitlik ve açıklık arıyorlar. Kendilerine anlatılanların arkasındaki öze inmeye çalışıyorlar.

8. Yukarıdaki maddeleri, zaman israfı olarak görüp, devamlı zamanlarını karmaşıklık ve yanlış nedenlerden dolayı batmakla harcayan insanlardan daha fazla zaman yaratabiliyorlar.
```





```
Bir takımda bireyler şu davranışları sergilemediğinde güven ortamı oluşmayacağı gibi, var olan da aşınır:

1. Eksik ve hataları söylemek

2. Verilen sözleri yerine getirmek

3. Başkalarının fikirlerine, kabul edilmese bile, saygı göstermek

Kalite azalmışsa, bunlara da bakın.
```

```
Fikirlerinizi savunmak ve size alternatif fikirler ile gelenlerin fikirlerini sorgulamak, ego olarak görülmemeli. 

Bunu, özellikle başkalarının fikirlerini olduğu gibi kabul eden, sonra kendi fikirlerini de başkalarının kabul etmesini bekleyen insanlar ego ile karıştırıyor.


Bu işin doğrusu, fikirlerinizi taşa yazmamak. Ama her gelen fikri de anlamadan ve sormadan kabul etmemek olacaktır. 

Fikirlerinizi değiştirmemeye gösterilen inatçılık ile doğruları anlamaya gösterilen inat aynı değildir.

İngilizce’de bu tenacious ile ifade edilir.

```

```

olmayani oldurmaya calismak hk.

halk dilinde olmayacak duaya amin demek. Bu sorunsali ancak alternatiflerin ne kadar etkili oldugunu fark ettiginizde asiliyor. kendi fikrine asik olmak gibi, tunelin sonunda gordugunu sandigin isik aslinda sana dogru gelen tren 😒


utanır mı sıkılır mı hic ?
vicdanın hic yok mu pic,

kalinir mi durulur mu hic ?
kendine saygin yok mu pic,

```





```
gelecek; onu gorene kadar sekilsizdir. Bir parayi havaya attiginda ya tura gelir ya da yazi. ama sen gorene kadar ikisi de degildir.
bilinmezlik, korkunun bir halidir. korkulari yenmenin tek yolu uzerine gitmektir. karanlikta oldugunu dusunuyorsan belki de gozlerini acmalisin ya da isigi yakmalisin. hazirlik ve aksiyon sonrasi para dik gelmedigi surece hep sen kazanacaksin. 🍻

```

