## Beden Dili notlar

```
Fark ettiğim ilk şeylerden biri, beni gerçekten seven öğrenci ya da öğretmenlerin, benimle karşılaştıkları zamanlarda kaşlarını kaldırdıklarıydı.

Diğer taraftan, bana karşı çok da arkadaşça hisler beslemeyen diğerleri, beni gördüklerinde belli belirsiz şekilde gözlerini kısıyordu


: “Görüyorsun ama gözlemlemiyorsun.” 


Örneğin, bir trafik kazasının ardından, insanların şok geçirmiş bir halde ve sersem bir şekilde etrafta dolanmaları beklenir. Ellerinin titremesi ve trafiğin ortasına yürümek gibi yanlış kararlar vermeleri de tahmin edilebilecek şeyler arasındadır (bir kazanın ardından memurların aracınızın içinde beklemenizi istemesi bundandır).


rneğin, karşınızdaki kişinin dudaklarını birleştirerek neredeyse yok olacak şekilde ağzının içine çekmesi, bu kişinin endişe duyduğunun ve bir şeylerin yanlış olduğunun açık ve genel bir işaretidir



Örneğin, 
ergenlik çağındaki çocuğunuzun herhangi bir sınav öncesinde kafasını kaşıdığını ve dudaklarını ısırdığını fark ederseniz, bu, çocuğunuzun gerginliğini ya da sınava hazırlanmadığını gösteren güvenilir bir idyosenkratik davranış olabilir.


"gelecekteki davranışlarımızın en açık göstergesi, geçmişteki davranışlanmızdır.”



Ancak normal olanın bilincinde olarak, anormal olanı algılayıp tespit edebiliriz.

Engelleme davranışları gözleri yummak, ovmak ya da ellerle yüzü kapatmak şeklinde ortaya koyulabilir.

Kişi temasa geçmek istemediği insanla arasına mesafe koymak için, gerilemek, kucağına bir şey almak (mesela bir çanta) ya da ayaklarını en yakın çıkışa doğru döndürmek gibi tepkilerde bulunabilir


Savaşma Tepkisi

Gözlerin bloke edilmesi şaşkınlık, güvensizlik ya da anlaşmazlığın güçlü bir göstergesidir

Limbik sistemin donma, kaçma ve savaşma tepkilerinin sözel olmayan davranışlarımızı nasıl etkilediğini anlamak denklemin yalnızca bir parçasıdır.


Kendisini yeniden “normal konumuna” geçirmek isteyen beynim iz, bedenimizi rahatlatıcı (yatıştırıcı) davranışlarda bulunm aya yönlendirir

Örneğin, kediler ve köpekler yatışmak için kendilerini ve birbirlerini yalar


çocukların başparm aklarını emmesini  çiklet çiğnemek, kalem ısırmak gibi


Rahatsızlık ifade eden davranışları (gerilem ek, kaşların çatılması, kolların kavuşturulması ya da gerilm esi gibi) genellikle beynin elleri yeniden rahat bir konum a geçirmesi izler


kendisine yönelttiğim sorulara olum suz yanıt verdiği zam anlarda karşım daki kişi boynuna ya da ağzına dokunuyorsa


Baskı altındaki kişi sigara kullanıcısıysa, daha çok sigara içmeye yönelecek; çiklet çiğniyorsa bunu daha hızlı bir şekilde yapmaya başlayacaktır. 

Kadın gerdanlığıyla oynamaya başlamışsa, bu büyük ihtimalle biraz gergin olduğunu gösterir. 
Ancak, parmaklarını boynunun altındaki çukur bölgeye götürürse, kendisini endişelendiren bir konunun konuşulduğunu ya da güvensiz bir durumda hissettiğini düşünebiliriz



```
Ayak ve Bacak Davranışları

