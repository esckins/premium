

In fact, I have improved myself many times in this field. My bilateral relations were always high as long as it was not official. But as you know, development is continuous.

At the end of the year party, it was suddenly my turn to speak. I was caught off guard, and then I noticed that my voice was shaking. Even though I look cute, I was uncomfortable with this situation. Below I will share tips and notes from the books I read to solve this problem.

* for a good talk

Firstly, It is necessary to know the spoken language well. Secondly, arouse curiosity and interest (silence for 2 seconds), thirtly, body language (hands crossed - trust) and then lastly to be timer and joke.

Generally speaking, it should be in the shape of a fishbone. Ex. We will talk about 3 main topics.

If they join the conversation with the other party, arouse curiosity and ask a question at the end, it means a quality conversation has taken place.

* Books-

Dale Carnagie - Söz söyleme ve İş başarma sanatı

Joe Navarro - Beden Dili

Cristian Stuart - Başarıya Giden Yolda Etkili Konuşma Yöntemleri

Most importantly, it is absolutely practical. Practice makes you perfect..

[Tips](https://professional.dce.harvard.edu/blog/10-tips-for-improving-your-public-speaking-skills/)



